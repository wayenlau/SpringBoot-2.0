package com.wayen.service.impl;

import com.wayen.mapper.RoleMapper;
import com.wayen.model.Role;
import com.wayen.model.RoleExample;
import com.wayen.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    RoleMapper roleMapper;

    @Cacheable(cacheNames = "user.service.list")
    @Override
    public List list() {
        RoleExample roleExample = new RoleExample();
        roleExample.setOrderByClause("name desc");
        List<Role> roles = roleMapper.selectByExample(roleExample);
        return roles;
    }

    @Override
    public void addUser() {
        Role role = new Role();
        role.setName("liuwei" + UUID.randomUUID().toString().replace("_", ""));
        roleMapper.insertSelective(role);
    }
}
