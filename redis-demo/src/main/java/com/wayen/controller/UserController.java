package com.wayen.controller;

import com.wayen.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping(value = "/user")
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/list")
    public List list() {
        List list = userService.list();
        return list;
    }

    @GetMapping("/add")
    public String add() {
        userService.addUser();
        return "SUCCESS";
    }
}
