package com.wayen.controller;

import com.wayen.service.TestService;
import com.wayen.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/test")
@RestController
public class TestController {

    @Autowired
    TestService testService;

    @Autowired
    RedisUtils redisUtils;
    @Autowired
    RedisTemplate redisTemplate;

    @GetMapping
    public String hello() {
        return testService.hello();
    }

    @GetMapping(value = "/redis/add")
    public String testRedis(String value) {
        redisUtils.set("test1", value, 2);
        return "SUCCESS";
    }

    @GetMapping(value = "/redis/get")
    public String getRedis(String key) {
        return redisUtils.get(key).toString();
    }
}
