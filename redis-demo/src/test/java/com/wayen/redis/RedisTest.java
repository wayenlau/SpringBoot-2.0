package com.wayen.redis;

import com.wayen.WayenApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.HyperLogLogOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WayenApplication.class)
public class RedisTest {

    @Autowired
    RedisTemplate redisTemplate;

    @Test
    public void testAdd() {
        HyperLogLogOperations<String, String> hyperLogLog = redisTemplate.opsForHyperLogLog();

        for (int i = 1; i < 500; i++) {
            hyperLogLog.add("hyper_log_liuwei", i + "");
            hyperLogLog.add("hyper_log_zhoujielun", i + "");
        }
        System.out.println("====添加成功====");
    }

    @Test
    public void testGet() {
        HyperLogLogOperations hyperLogLogOperations = redisTemplate.opsForHyperLogLog();
        Long log_liuwei = hyperLogLogOperations.size("hyper_log_liuwei");
        Long hyper_log_zhoujielun = hyperLogLogOperations.size("hyper_log_zhoujielun");
        System.out.println(log_liuwei);
        System.out.println(hyper_log_zhoujielun);

    }
}
