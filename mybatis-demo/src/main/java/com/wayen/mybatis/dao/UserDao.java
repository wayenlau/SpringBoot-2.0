package com.wayen.mybatis.dao;

import com.wayen.entity.User;
import com.wayen.entity.UserExample;
import com.wayen.entity.po.SelectUnionParamter;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

public interface UserDao {


    List<User> selectByExample(UserExample example);

    List<User> selectAll();

    List<User> selectUnion(List<String> list);

    List<User> selectUnionParamter(SelectUnionParamter paramter);
}