package com.wayen.entity;

import java.io.Serializable;

public class User1 implements Serializable {
    /**
     * 
     */
    private Integer id;

    /**
     * 
     */
    private String user_name;

    /**
     * 
     */
    private Integer age;

    /**
     * 
     */
    private String address;

    /**
     * 昵称
     */
    private String nick_name;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public User1 withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUser_name() {
        return user_name;
    }

    public User1 withUser_name(String user_name) {
        this.setUser_name(user_name);
        return this;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public Integer getAge() {
        return age;
    }

    public User1 withAge(Integer age) {
        this.setAge(age);
        return this;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public User1 withAddress(String address) {
        this.setAddress(address);
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNick_name() {
        return nick_name;
    }

    public User1 withNick_name(String nick_name) {
        this.setNick_name(nick_name);
        return this;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", user_name=").append(user_name);
        sb.append(", age=").append(age);
        sb.append(", address=").append(address);
        sb.append(", nick_name=").append(nick_name);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}