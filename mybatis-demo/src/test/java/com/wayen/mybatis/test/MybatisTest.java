package com.wayen.mybatis.test;

import com.wayen.MybatisDemoApplication;
import com.wayen.entity.User;
import com.wayen.entity.User1;
import com.wayen.entity.User1Example;
import com.wayen.entity.UserExample;
import com.wayen.entity.po.SelectUnionParamter;
import com.wayen.mybatis.dao.UserDao;
import com.wayen.mybatis.mapper.User1Mapper;
import com.wayen.mybatis.mapper.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@SpringBootTest(classes = MybatisDemoApplication.class)
@RunWith(SpringRunner.class)
public class MybatisTest {

    @Autowired
    UserMapper userMapper;

    @Test
    public void test() {
        UserExample example = new UserExample();
        example.createCriteria();
        List<User> users = userMapper.selectByExample(example);
        for (User user : users) {
            System.out.println("name=" + user.getUserName());
            System.out.println("age=" + user.getAge());
            System.out.println("address=" + user.getAddress());
            System.out.println("============================");
        }

    }


    @Autowired
    UserDao userDao;

    @Test
    public void test2() {
        UserExample example = new UserExample();
        example.createCriteria();
        List<User> users = userDao.selectByExample(example);
        for (User user : users) {
            System.out.println("name=" + user.getUserName());
            System.out.println("age=" + user.getAge());
            System.out.println("address=" + user.getAddress());
            System.out.println("============================");
        }
    }


    @Test
    public void test3() {
        List<User> users = userDao.selectAll();
        for (User user : users) {
            System.out.println("name=" + user.getUserName());
            System.out.println("age=" + user.getAge());
            System.out.println("address=" + user.getAddress());
            System.out.println("============================");
        }
    }

    @Test
    public void test4() {
        ArrayList<String> list = new ArrayList<>();
        list.add("user2");
        List<User> users = userDao.selectUnion(list);
        for (User user : users) {
            System.out.println("name=" + user.getUserName());
            System.out.println("age=" + user.getAge());
            System.out.println("address=" + user.getAddress());
            System.out.println("============================");
        }
    }

    @Test
    public void test5() {
        SelectUnionParamter paramter = new SelectUnionParamter();
        paramter.setName("刘");
        ArrayList<String> list = new ArrayList<>();
        list.add("user2");
        paramter.setList(list);
        List<User> users = userDao.selectUnionParamter(paramter);
        for (User user : users) {
            System.out.println("name=" + user.getUserName());
            System.out.println("age=" + user.getAge());
            System.out.println("address=" + user.getAddress());
            System.out.println("============================");
        }
    }

    @Autowired
    User1Mapper user1Mapper;

    @Test
    public void test6() {
        User1 user1 = new User1();
        user1.setUser_name("刘伟");
        user1.setAddress("123456");
        user1.setAge(18);
        user1.setNick_name("  ");
        user1Mapper.insertSelective(user1);

        User1 user2 = new User1();
        user2.setUser_name("刘伟2");
        user2.setAddress("1234562");
        user2.setAge(182);
        user2.setNick_name("user2");
        user1Mapper.insertSelective(user2);

        User1 user3 = new User1();
        user3.setUser_name("刘伟3");
        user3.setAddress("1234563");
        user3.setAge(183);
        user1Mapper.insertSelective(user3);
    }

    @Test
    public void test7() {
        List<User1> user1s = user1Mapper.selectByExample(new User1Example());
        for (User1 user :
                user1s) {
            System.out.println(user.getUser_name());
            System.out.println(user.getNick_name());
            System.out.println("==========================");
        }

    }
}
