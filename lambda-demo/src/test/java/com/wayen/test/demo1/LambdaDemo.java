package com.wayen.test.demo1;

import com.wayen.LambdaDemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LambdaDemoApplication.class)
public class LambdaDemo {

    @Test
    public void test1() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("匿名内部类实现线程......");
            }
        }).start();

        new Thread(() -> System.out.println("lambda表达式实现线程.........")).start();
    }

    @Test
    public void test2() {

    }

}
