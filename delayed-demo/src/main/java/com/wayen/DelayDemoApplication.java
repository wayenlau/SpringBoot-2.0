package com.wayen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DelayDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DelayDemoApplication.class, args);
    }
}
