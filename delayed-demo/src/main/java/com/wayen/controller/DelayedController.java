package com.wayen.controller;

import com.wayen.delayed.Consumer;
import com.wayen.delayed.Message;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RequestMapping("/delayed")
@RestController
public class DelayedController {


    @GetMapping("/test")
    public String test(String message) {

        return "hello_" + message;
    }

}
