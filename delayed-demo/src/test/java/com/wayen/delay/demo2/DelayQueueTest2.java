package com.wayen.delay.demo2;

import com.wayen.delay.demo1.Message;

import java.util.concurrent.DelayQueue;

public class DelayQueueTest2 {

    public static void main(String[] args) {
        // 创建延时队列
        DelayQueue<Message> queue = new DelayQueue<Message>();
        // 添加延时消息,m1 延时3s
        Message m1 = new Message(1, "world", 3000);
        // 添加延时消息,m2 延时10s
        Message m2 = new Message(2, "hello", 50000);
        Message m3 = new Message(3, "java", 30000);
        //将延时消息放到延时队列中
        queue.offer(m2);
        queue.offer(m1);
        queue.offer(m3);

        while (queue.size() > 0) {
            try {
                Message take = queue.take();
                System.out.println("=================================");
                System.out.println("消息id= " + take.getId());
                System.out.println("消息内容= " + take.getBody());
                System.out.println("=================================");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
