package com.wayen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityLearnChapter1Application {

    public static void main(String[] args) {
        SpringApplication.run(SecurityLearnChapter1Application.class, args);
    }
}
