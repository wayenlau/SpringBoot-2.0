package com.wayen;

import com.wayen.entity.Sender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringBootTest(classes = RabbitMqApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class RabbitMqTest {

    @Autowired
    private Sender sender;

    @Test
    public void hello(){
        sender.send();
    }
}
