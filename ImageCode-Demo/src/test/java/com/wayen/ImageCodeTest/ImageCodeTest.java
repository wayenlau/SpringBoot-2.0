package com.wayen.ImageCodeTest;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.wayen.ImageCodeDempApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@SpringBootTest(classes = ImageCodeDempApplication.class)
@RunWith(SpringRunner.class)
public class ImageCodeTest {

    @Autowired
    DefaultKaptcha defaultKaptcha;

    @Test
    public void testGetTmageCode() {
        for (int i = 0; i < 10; i++) {
            String text = defaultKaptcha.createText();
            System.out.println("code：【" + text + "】");
            BufferedImage image = defaultKaptcha.createImage(text);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();//io流
            try {
                ImageIO.write(image, "png", baos);//写入流中
                byte[] bytes = baos.toByteArray();//转换成字节
                BASE64Encoder encoder = new BASE64Encoder();
                String png_base64 = encoder.encodeBuffer(bytes).trim();//转换成base64串
                png_base64 = png_base64.replaceAll("\n", "").replaceAll("\r", "");//删除 \r\n
                System.out.println("data:image/png;base64," + png_base64);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}
