package com.wayen.controller;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.wayen.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class ImageCodeController {

    @Autowired
    DefaultKaptcha defaultKaptcha;

    @Autowired
    RedisUtils redisUtils;

    @GetMapping("/image/code")
    public Map<String, String> getImageCode() {

        Map<String, String> map = new HashMap<>();

        //生产验证码字符串
        String code = defaultKaptcha.createText();
        //使用生产的验证码字符串返回一个BufferedImage对象
        BufferedImage image = defaultKaptcha.createImage(code);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();//io流
        try {
            ImageIO.write(image, "png", baos);//写入流中
            byte[] bytes = baos.toByteArray();//转换成字节
            BASE64Encoder encoder = new BASE64Encoder();
            String png_base64 = encoder.encodeBuffer(bytes).trim();//转换成base64串
            png_base64 = png_base64.replaceAll("\n", "").replaceAll("\r", "");//删除 \r\n
            String substring = new StringBuffer(png_base64).substring(png_base64.length() - 10);
            map.put("data", png_base64);
            map.put("key", substring);
            redisUtils.set(substring, code);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return map;
    }
}
