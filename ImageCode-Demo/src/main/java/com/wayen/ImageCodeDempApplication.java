package com.wayen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImageCodeDempApplication {

    public static void main(String[] args){
        SpringApplication.run(ImageCodeDempApplication.class,args);
    }

}
