package com.wayen.controller;

import com.wayen.mq.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MqTestController {

    @Autowired
    Sender sender;

    @GetMapping("/mq")
    public String mq(String s) {
        return sender.send(s);
    }
}
