package com.wayen.design.model.CommandPattern.demo2;

/**
 * 抽象命令
 */
public interface Command {
    public abstract void execute();
}
