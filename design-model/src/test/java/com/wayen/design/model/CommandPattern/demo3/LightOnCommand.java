package com.wayen.design.model.CommandPattern.demo3;

public class LightOnCommand implements Command {

    private Receiver receiver;
    /**
     *
     */
    public LightOnCommand(Receiver receiver) {
        this.receiver = receiver;
    }
    /**
     * @see com.pichen.dp.behavioralpattern.command.Command#execute()
     */
    @Override
    public void execute() {
        receiver.turnON();
    }
}
