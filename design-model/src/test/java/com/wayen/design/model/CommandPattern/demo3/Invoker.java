package com.wayen.design.model.CommandPattern.demo3;

/**
 * 命令调用者 负责调用
 */
public class Invoker {

    public void execute(Command command){
        command.execute();
    }
}
