package com.wayen.design.model.CommandPattern.demo3;

/**
 * 抽象命令
 */
public interface Command {
    public void execute();
}
