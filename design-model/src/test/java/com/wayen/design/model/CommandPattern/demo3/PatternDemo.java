package com.wayen.design.model.CommandPattern.demo3;

/**
 * 客户端：开关灯例子
 */
public class PatternDemo {

    public static void main(String[] args) {
        //命令接受者
        Receiver receiver = new Receiver();
        //命令调用者
        Invoker invoker = new Invoker();
        //具体命令
        Command turnOnLight = new LightOnCommand(receiver);
        Command turnOffLight = new LightOffCommand(receiver);

        invoker.execute(turnOnLight);
        invoker.execute(turnOffLight);
    }
}
