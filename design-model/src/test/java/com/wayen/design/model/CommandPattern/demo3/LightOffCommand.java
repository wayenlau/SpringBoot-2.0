package com.wayen.design.model.CommandPattern.demo3;

/**
 * 具体命令 开灯和关灯
 */
public class LightOffCommand implements Command {
    private Receiver receiver;
    /**
     *
     */
    public LightOffCommand(Receiver receiver) {
        this.receiver = receiver;
    }
    /**
     * @see com.pichen.dp.behavioralpattern.command.Command#execute()
     */
    @Override
    public void execute() {
        this.receiver.turnOFF();
    }
}
