package com.wayen.design.model.CommandPattern.demo3;

/**
 * 命令接收者 命令接受者：负责执行具体命令操作
 */
public class Receiver {

    public void turnON() {
        System.out.println("执行开灯操作~");
    }
    public void turnOFF() {
        System.out.println("执行关灯操作~");
    }
}
