package com.wayen.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("/hello")
    public String hello() {
        return "hello A5F402C9CEA694E2684851DBDD4EB3A7 B646BE14C8E87EFDCE39C87846B05210 BBA034E0A45727F879744833CFFC3751";
    }
}
