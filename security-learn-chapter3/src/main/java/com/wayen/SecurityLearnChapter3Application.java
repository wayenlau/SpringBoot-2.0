package com.wayen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityLearnChapter3Application {

    public static void main(String[] args) {
        SpringApplication.run(SecurityLearnChapter3Application.class, args);
    }
}
