package com.wayen.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;

@Controller
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @GetMapping("/authentication/login")
    public String authenticationLogin() throws IOException {
        logger.info("=====================跳转登录页======================================");
        return "login";
    }

}
