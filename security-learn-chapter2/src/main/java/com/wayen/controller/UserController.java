package com.wayen.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/user")
@RestController
public class UserController {

    @GetMapping("/info")
    public String info() {
        return "我是周杰伦";
    }

    @GetMapping("delete")
    public String delete() {
        return "删除用户";
    }
}
