package com.wayen.jpa;

import com.wayen.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserJpa extends JpaRepository<Users, Long> {

}
