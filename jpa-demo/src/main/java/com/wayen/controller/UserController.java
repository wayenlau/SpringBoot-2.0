package com.wayen.controller;

import com.wayen.entity.Users;
import com.wayen.jpa.UserJpa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RequestMapping("/user")
@RestController
public class UserController {

    @Autowired
    UserJpa userJpa;

    @GetMapping("/all")
    public List<Users> getAll() {
        return userJpa.findAll();
    }

    @GetMapping("/add")
    public String addUsers(String username, String password) {
        Users users = new Users();
        users.setUserName(username);
        users.setPassWord(password);
        userJpa.save(users);
        return "SUCCESS";
    }

    @GetMapping("/delete")
    public String delete(Integer id) {
        Users users = new Users();
        users.setId(Long.parseLong(id + ""));
        userJpa.delete(users);
        return "SUCCESS";
    }

    @GetMapping("/getById")
    public Users getById(Long id) {
        Optional<Users> byId = userJpa.findById(id);
        Users users = byId.get();
        return users;
    }
}
