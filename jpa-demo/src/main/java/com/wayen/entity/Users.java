package com.wayen.entity;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "users")
@Entity
public class Users implements Serializable {

    /**
     * GeneratedValue主键的产生策略
     *      AUTO
     *      IDENTITY
     *      SEQUENCE
     *      TABLE
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "u_id")
    private Long id;

    @Column(name = "u_username", length = 50)
    private String userName;

    @Column(name = "u_password", length = 255)
    private String passWord;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
}
