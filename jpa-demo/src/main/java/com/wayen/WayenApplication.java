package com.wayen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 说明：启动类
 *
 */
@SpringBootApplication
public class WayenApplication {

    public static void main(String[] args) {
        SpringApplication.run(WayenApplication.class, args);
    }

}