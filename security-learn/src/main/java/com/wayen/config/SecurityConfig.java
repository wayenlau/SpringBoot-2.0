package com.wayen.config;

import com.wayen.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 通过重载配置SpringSecurity的Filter链
     *
     * @param web
     * @throws Exception
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        super.configure(web);
    }

    /**
     * 通过重载配置如何通过拦截器保护请求
     * authenticated()要求在执行该请求是必须已经登录
     * permitAll()要求请求没有任何登录限制
     *
     * anonymous()允许匿名访问
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/user/**").authenticated() //配置/user/**的请求需要验证
                .anyRequest().permitAll()                             //配置其他的请求都不需要验证
                .and().formLogin()
                .loginPage("/login.html")
                .loginProcessingUrl("/mmm")
                .and().httpBasic()
                .and().csrf().disable();
    }

    /**
     * 通过重载配置user-detail服务
     *
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        /*
        auth.inMemoryAuthentication()
                .withUser("user").password("password").roles("USER")
                .and()
                .withUser("admin").password("password").roles("USER","ADMIN");
        */
        auth.userDetailsService(new UserService());
    }


    @Bean
    protected BCryptPasswordEncoder passwordEncoder() {
        return new MyPasswordEncoder();
    }
}
