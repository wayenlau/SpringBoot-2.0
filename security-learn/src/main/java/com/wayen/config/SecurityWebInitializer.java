package com.wayen.config;


import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * 注册DelegatingFilterProxy
 *  拦截发往应用中的请求并将请求委托给SpringSecurityFilterChain
 */
public class SecurityWebInitializer extends AbstractSecurityWebApplicationInitializer {



}
