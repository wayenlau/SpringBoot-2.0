package com.wayen.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/user")
@RestController
public class UserController {

    @GetMapping("/get")
    public String getUser(){
        return "获取用户信息成功";
    }

    @GetMapping("/delete")
    public String deleteUser(){
        return "删除用户成功";
    }
}
