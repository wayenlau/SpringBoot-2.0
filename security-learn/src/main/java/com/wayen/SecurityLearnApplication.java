package com.wayen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * Security教程 https://blog.csdn.net/lizc_lizc/article/details/84059004
 */
@SpringBootApplication
public class SecurityLearnApplication {

    public static void main(String[] args){
        SpringApplication.run(SecurityLearnApplication.class,args);
    }
}
