package com.wayen.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    private static final Logger logger= LoggerFactory.getLogger(TestController.class);

    @GetMapping("/hello")
    public String hello(){
        return "hello_word";
    }

    @GetMapping("/products/get")
    public String productsGet(){
        logger.info("GET方法======>");
        return "products";
    }

    @DeleteMapping("/products/delete")
    public String productsDelete(){
        logger.info("DELETE方法======>");
        return "删除成功";
    }
}
