package com.wayen.DateTest;

import com.wayen.SpringBootStartApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2019/5/28.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootStartApplication.class)
public class DateTest {

    @Test
    public void test() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String format = simpleDateFormat.format(date);
        System.out.println("==============================================================");
        System.out.println(format);
    }

    @Test
    public void test2() {
        long i = 1557490285846l;
        Date date = new Date(i);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = simpleDateFormat.format(date);
        System.out.println("==============================================================");
        System.out.println(format);

    }

    @Test
    public void test3(){
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = simpleDateFormat.format(date);
        System.out.println("==============================================================");
        System.out.println(format);
    }

    @Test
    public void test4(){
        long i=1566814272534l;
        Date date = new Date(i);
        System.out.println(date);

    }

}
