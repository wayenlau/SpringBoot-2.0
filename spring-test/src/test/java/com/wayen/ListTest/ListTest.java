package com.wayen.ListTest;

import com.alibaba.fastjson.JSONObject;
import com.wayen.SpringBootStartApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootStartApplication.class)
public class ListTest {

    @Test
    public void test() {
        ArrayList<String> list1 = new ArrayList<>();
        String[] strings = new String[]{"hello", "world", "123", "456"};
        List<String> list = Arrays.asList(strings);
        for (String s : list) {
            System.out.println(s);
        }
        System.out.println(list.toString());
        System.out.println(JSONObject.toJSONString(list));
    }

    @Test
    public void test2() {
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                continue;
            }
            System.out.println(i);
        }

    }


    /**
     * 取交集
     */
    @Test
    public void test3() {
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");


        List<String> list1 = new ArrayList<>();
        list1.add("1");
        list1.add("2");

        list.retainAll(list1);

        for (String s: list) {
            System.out.println(s);
        }


    }
}
