package com.wayen.Decimal;

import com.wayen.SpringBootStartApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootStartApplication.class)
public class BigDecimalTest {

    @Test
    public void test1(){
        System.out.println(0.05 + 0.01);
        System.out.println(1.0 - 0.42);
        System.out.println(4.015 * 100);
        System.out.println(123.3 / 100);
    }

    /**
     *   构造器                 描述
     *   BigDecimal(int)       创建一个具有参数所指定整数值的对象。
     *   BigDecimal(double)    创建一个具有参数所指定双精度值的对象。
     *   BigDecimal(long)      创建一个具有参数所指定长整数值的对象。
     *   BigDecimal(String)    创建一个具有参数所指定以字符串表示的数值的对象。
     *
     *   方法                    描述
     *   add(BigDecimal)       BigDecimal对象中的值相加，然后返回这个对象。
     *   subtract(BigDecimal)  BigDecimal对象中的值相减，然后返回这个对象。
     *   multiply(BigDecimal)  BigDecimal对象中的值相乘，然后返回这个对象。
     *   divide(BigDecimal)    BigDecimal对象中的值相除，然后返回这个对象。
     *   toString()            将BigDecimal对象的数值转换成字符串。
     *   doubleValue()         将BigDecimal对象中的值以双精度数返回。
     *   floatValue()          将BigDecimal对象中的值以单精度数返回。
     *   longValue()           将BigDecimal对象中的值以长整数返回。
     *   intValue()            将BigDecimal对象中的值以整数返回。
     *
     */

    /**
     * 我们在使用BigDecimal时，使用它的BigDecimal(String)构造器创建对象才有意义。
     * 其他的如BigDecimal b = new BigDecimal(1)这种，还是会发生精度丢失的问题。
     * 声明BigDecimal对象的时候一定要使用它构造参数为String的类型的构造器。
     */
    @Test
    public void test2(){
        BigDecimal a = new BigDecimal(1.01);
        BigDecimal b = new BigDecimal(1.02);
        BigDecimal c = new BigDecimal("1.01");
        BigDecimal d = new BigDecimal("1.02");
        System.out.println(a.add(b));
        System.out.println(c.add(d));
        //输出：
        //2.0300000000000000266453525910037569701671600341796875
        //2.03
    }

    @Test
    public void test3(){
        BigDecimal decimal1 = new BigDecimal(5.23);
        BigDecimal decimal2 = new BigDecimal(5.23);

        int i = decimal1.compareTo(decimal2);
        System.out.println(i);
    }


}
