package com.wayen.AddressTest;


import com.wayen.SpringBootStartApplication;
import com.wayen.util.AddressUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.UnsupportedEncodingException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootStartApplication.class)
public class AddressTest {

    @Test
    public void test(){

        AddressUtils addressUtils = new AddressUtils();
        String ip = "111.230.138.137";
        String address = "";
        try {
            address = addressUtils.getAddresses("ip=" + ip, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println(address);

    }
}
