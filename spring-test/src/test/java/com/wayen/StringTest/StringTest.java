package com.wayen.StringTest;


import com.wayen.SpringBootStartApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootStartApplication.class)
public class StringTest {
    //https://wayenbucket1-1257014484.cos.ap-guangzhou.myqcloud.com/e6719834-99c3-482c-9ba1-81be46c0bc85timg%20(6).jpg
    @Test
    public void test() {
        String url = "https://wayenbucket1-1257014484.cos.ap-guangzhou.myqcloud.com/e6719834-99c3-482c-9ba1-81be46c0bc85timg%20(6).jpg";
        String[] split = url.split("/");
        String s = split[split.length - 1];
        System.out.println(s);
    }
}
