package com.wayen.EnumTest;

import com.wayen.SpringBootStartApplication;
import com.wayen.enumeration.OrderEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootStartApplication.class)
public class EnumTest {

    @Test
    public void test() {
        Byte status = OrderEnum.ORDER_ALREADY_CANCEL.status();
        System.out.println(status);
    }

}
