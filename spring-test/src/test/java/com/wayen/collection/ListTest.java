package com.wayen.collection;

import com.wayen.SpringBootStartApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2019/5/30.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootStartApplication.class)
public class ListTest {

    @Test
    public void test1() {
        List<String> listA = new ArrayList<String>();
        List<String> listB = new ArrayList<String>();
        listA.add("A");
        listA.add("B");
        listB.add("B");
        listB.add("C");

        //取交集
        listA.retainAll(listB);
        System.out.println(listA);

        //取并集
        // 不做第一步取的是有重复元素的并集
        listA.removeAll(listB);
        listA.addAll(listB);
        System.out.println(listA);

        //取差集
        listA.removeAll(listB);
        System.out.println(listA);

    }

    @Test
    public void test2(){
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        System.out.println(list);

    }
}
