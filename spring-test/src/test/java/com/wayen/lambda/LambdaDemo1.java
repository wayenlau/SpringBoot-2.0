package com.wayen.lambda;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootTest.class)
public class LambdaDemo1 {

    private static final Logger logger = LoggerFactory.getLogger(LambdaDemo1.class);

    @Test
    public void test1() {
        logger.info("主线程====>{}", Thread.currentThread().getName());
        // Java 8之前：
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Before Java8, too much code for too little to do");
                logger.info("多线程===={}", Thread.currentThread().getName());
            }
        }).start();

    }

    @Test
    public void test2() {
        logger.info("主线程====>{}", Thread.currentThread().getName());
        new Thread( () -> System.out.println("In Java8, Lambda expression rocks !!") ).start();

    }


}
