package com.wayen.test;

import com.wayen.SpringBootStartApplication;
import freemarker.template.SimpleHash;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2019/5/28.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootStartApplication.class)
public class TestDemo {

    @Test
    public void testDecemal() {

        BigDecimal bigDecimal = new BigDecimal(1.22);
        BigDecimal bigDecimal1 = new BigDecimal(5.66);
        BigDecimal divide = bigDecimal1.subtract(bigDecimal);
        System.out.println(divide);

    }

    /**
     * 生成验证码
     */
    @Test
    public void testRandomCode1() {
        for (int i = 0; i < 100; i++) {
            String random = RandomStringUtils.random(4, "0123456789");
            System.out.println(i + "====>" + random);
        }
    }

    @Test
    public void testRandomCode2() {
        int authCodeNew = 0;
        for (int i = 0; i < 100; i++) {
            authCodeNew = (int) Math.round(Math.random() * (9999 - 1000) + 1000);
            System.out.println(i + "====>" + authCodeNew);
        }
    }

    @Test
    public void testVaildPhone() {
        String phone = "13268006185";
        boolean b = isPhone(phone);
        System.out.println(b);
    }

    /**
     * 验证手机号码
     *
     * @param phone
     * @return
     */
    public static boolean isPhone(String phone) {
        String regex = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(166)|(17[0,1,3,5,6,7,8])|(18[0-9])|(19[8|9]))\\d{8}$";
        if (phone.length() != 11) {
            return false;
        } else {
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(phone);
            boolean isMatch = m.matches();
            if (!isMatch) {
                return false;
            }
            return isMatch;
        }
    }

    @Test
    public void test2() {
        Integer i = 13;
        Integer z = i % 3;
        System.out.println(z);
    }

    @Test
    public void test7(){
        String s=" ";
        boolean notBlank = StringUtils.isNotBlank(s);
        System.out.println(notBlank);
    }

}
