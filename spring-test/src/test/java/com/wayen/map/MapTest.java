package com.wayen.map;

import com.wayen.SpringBootStartApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;

@SpringBootTest(classes = SpringBootStartApplication.class)
@RunWith(SpringRunner.class)
public class MapTest {

    @Test
    public void test(){
        HashMap<Object, Object> map = new HashMap<>();

        String s="123";
        int i = s.hashCode();
        System.out.println(i);

        ArrayList<Object> list = new ArrayList<>();
        int i1 = list.hashCode();
        System.out.println(i1);
    }
}
