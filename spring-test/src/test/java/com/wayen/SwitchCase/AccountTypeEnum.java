package com.wayen.SwitchCase;

/**
 * 科目(
 * 0：其他，
 * 1：订单入账，
 * 2：商家退款，
 * 3：充值，
 * 4：提现，
 * 5：提现失败退回，
 * 6：微信支付，
 * 7：运费分账，
 * 8：退保，
 * 9：违规扣款，
 * 10：交易手续费，
 * 11：信息技术服务费，
 * 12：发票运费，
 * 13：分享有奖，
 * 14：交保，
 * 15：信息技术服务费_平台，
 * 16：提现_平台，
 * 17：违规扣款_平台，
 * 18：交易手续费_平台，
 * 19：发票运费_平台，
 * 20：分享有奖_平台)
 */
public enum AccountTypeEnum {
    OTHER(0, "其他"),
    ORDER(1, "订单入账"),
    REFUND(2, "商家退款");

    private Integer code;
    private String msg;

    AccountTypeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    AccountTypeEnum(Integer code) {
        this.code = code;
        switch (code) {
            case 0: {
                this.msg = "其他";
                break;
            }
            case 1: {
                this.msg = "订单入账";
                break;
            }
            case 2: {
                this.msg = "商家退款";
                break;
            }
        }
    }
}
