package com.wayen.SwitchCase;

import com.wayen.SpringBootStartApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootStartApplication.class)
public class SwitchCaseTest {

    @Test
    public void test() {

        int i = 1;

        switch (i) {
            case 1: {
                System.out.println("1号");
                break;
            }
            case 2: {
                System.out.println("2号");
                break;
            }
            case 3: {
                System.out.println("3号");
                break;
            }


        }
    }

    @Test
    public void test2() {
        int i = 1;

    }
}
