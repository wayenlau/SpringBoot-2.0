package com.wayen.IntegerTest;

import com.wayen.WayenApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WayenApplication.class)
public class IntegerTest {

    @Test
    public void test(){
        String s="169.89";
        int i = Integer.parseInt(s);
        System.out.println(i);
    }
}
