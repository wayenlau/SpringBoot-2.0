package com.wayen.controller;

import com.alibaba.fastjson.JSONObject;
import com.google.common.io.Resources;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.http.HttpMethodName;
import com.qcloud.cos.model.GeneratePresignedUrlRequest;
import com.qcloud.cos.model.ObjectMetadata;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.region.Region;
import com.wayen.mapper.EmployeeMapper;
import com.wayen.mapper.UsersMapper;
import com.wayen.model.*;
import com.wayen.service.TestService;
import com.wayen.util.RedisUtils;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("SpringJavaAutowiringInspection")
@RequestMapping(value = "/test")
@RestController
public class TestController {

    private static final Logger logger = LoggerFactory.getLogger(TestController.class);

    @Autowired
    TestService testService;

    @GetMapping("/1")
    public String test1() {
        return "hello";
    }

    @GetMapping("/hello")
    public String hello(String name, int age) {
        return testService.hello(name, age);
    }

    /**
     * Session和Cookie测试
     *
     * @param request
     * @param response
     * @return
     */
    @GetMapping("/session")
    public String session(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        if (session.getAttribute("test") != null) {
            System.out.println("test-session========================================");
            System.out.println(session.getAttribute("test").toString());
            System.out.println("====================================================");
        } else {
            session.setAttribute("test", "wayen-session");
        }

        // new一个Cookie对象,键值对为参数name--->value
        Cookie cookie = new Cookie("demo", "wayen-session");
        // tomcat下多应用共享
        cookie.setPath("/");
        // 如果cookie的值中含有中文时，需要对cookie进行编码，不然会产生乱码
        try {
            URLEncoder.encode("demo", "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        cookie.setMaxAge(10000);//设置有效时间
        // 将Cookie添加到Response中,使之生效
        response.addCookie(cookie); // addCookie后，如果已经存在相同名字的cookie，则最新的覆盖旧的cookie
        return "success";
    }


    /**
     * SECRET_ID=AKIDNp32GFlXnolTwLeqjMkskMo8D2bv9pOm
     * SECRET_KEY=PZYHm22mMo0xe6IEuQvA9sMaYGoEPK0S
     * TEST_BUCKET=test-1259235038
     *
     * @param file
     * @return
     */
    @ApiOperation(value = "图片上传")
    @PostMapping("/img")
    public String uploadImg(@RequestParam("file") MultipartFile file) {
        // 1 初始化用户身份信息（secretId, secretKey）。
        COSCredentials cred = new BasicCOSCredentials("AKIDiOxNdy8JE2l7NSxbR6Zq5A4Ys32qx3Re", "51nQs96tUnwLZpxwjNrFj4lGftvRbaRi");
        // 2 设置bucket的区域, COS地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        // clientConfig中包含了设置 region, https(默认 http), 超时, 代理等 set 方法, 使用可参见源码或者常见问题 Java SDK 部分。
        ClientConfig clientConfig = new ClientConfig(new Region("ap-guangzhou"));
        // 3 生成 cos 客户端。
        COSClient cosClient = new COSClient(cred, clientConfig);
        // bucket的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
        String bucketName = "wayenbucket1-1257014484";
        String key = UUID.randomUUID().toString() + file.getOriginalFilename();
        if (!file.isEmpty()) {
            try {
                FileInputStream fileInputStream = null;
                InputStream in = file.getInputStream();
                if (in instanceof FileInputStream) {
                    try {
                        fileInputStream = (FileInputStream) in;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                // 从输入流上传(需提前告知输入流的长度, 否则可能导致 oom)
                ObjectMetadata objectMetadata = new ObjectMetadata();
                // 设置输入流长度为500
                objectMetadata.setContentLength(file.getSize());
                // 设置 Content type, 默认是 application/octet-stream
                objectMetadata.setContentType("image/jpeg");
                PutObjectResult putObjectResult = cosClient.putObject(bucketName, key, fileInputStream, objectMetadata);

                GeneratePresignedUrlRequest req =
                        new GeneratePresignedUrlRequest(bucketName, key, HttpMethodName.GET);
                URL url = cosClient.generatePresignedUrl(req);

                return url.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @PostMapping("/testPost")
    public String testPost(String hello) {
        return hello;
    }

    @Autowired
    UsersMapper usersMapper;

    @GetMapping("/getUser")
    public String getUser() {
        UsersExample usersExample = new UsersExample();
        List<Users> users = usersMapper.selectByExample(usersExample);
        String s = JSONObject.toJSONString(users);
        return s;
    }

    @Autowired
    EmployeeMapper employeeMapper;

    @GetMapping("/getEmployee")
    public String getEmployee() {
        EmployeeExample employeeExample = new EmployeeExample();
        List<Employee> employees = employeeMapper.selectByExample(employeeExample);
        String s = JSONObject.toJSONString(employees);
        return s;
    }

    @Autowired
    Environment environment;

    @PostMapping("/addEmployee")
    public String addEmployee(@RequestBody Employee employee) {
        logger.info("TestController.addEmployee====>" + JSONObject.toJSONString(employee));
        employeeMapper.insertSelective(employee);
        //获取配置文件值
        String s = environment.getProperty("server.port", "哈哈哈哈哈");
        return s;
    }

    @Autowired
    MyBean myBean;

    @GetMapping("/mybean")
    public String mybean() {
        return myBean.doSomthing();
    }

    @Autowired
    RedisUtils redisUtils;

    @GetMapping("/addUser")
    public String addUser(String userName) {
        try {


        } catch (Exception e) {
        }
        long time = new Date().getTime();
        User user = new User();
        user.setName(userName);
        user.setAge(10);
        redisUtils.set("" + time, user);
        return "" + time;
    }
}
