package com.wayen.controller;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.http.HttpMethodName;
import com.qcloud.cos.model.GeneratePresignedUrlRequest;
import com.qcloud.cos.model.ObjectMetadata;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.region.Region;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.UUID;

@RestController
public class ImageController {

    String secretId = "AKIDiOxNdy8JE2l7NSxbR6Zq5A4Ys32qx3Re";
    String secretKey = "51nQs96tUnwLZpxwjNrFj4lGftvRbaRi";
    String bucketName = "wayenbucket1-1257014484";

    @SuppressWarnings("Duplicates")
    @ApiOperation(value = "图片上传阿里云服务器")
    @PostMapping("/uploadImg")
    public String uploadImg2(@RequestParam("file") MultipartFile file) {
        long size = file.getSize();
        // 1 初始化用户身份信息（secretId, secretKey）。
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        // 2 设置bucket的区域, COS地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        // clientConfig中包含了设置 region, https(默认 http), 超时, 代理等 set 方法, 使用可参见源码或者常见问题 Java SDK 部分。
        ClientConfig clientConfig = new ClientConfig(new Region("ap-guangzhou"));
        // 3 生成 cos 客户端。
        COSClient cosClient = new COSClient(cred, clientConfig);
        // bucket的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
//        String bucketName = "wayenbucket1-1257014484";
//        String key = UUID.randomUUID().toString() + file.getOriginalFilename();
        String key = file.getOriginalFilename();
        if (!file.isEmpty()) {
            try {
                FileInputStream fileInputStream = null;
                InputStream in = file.getInputStream();
                if (in instanceof FileInputStream) {
                    try {
                        fileInputStream = (FileInputStream) in;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                // 从输入流上传(需提前告知输入流的长度, 否则可能导致 oom)
                ObjectMetadata objectMetadata = new ObjectMetadata();
                // 设置输入流长度为500
                objectMetadata.setContentLength(file.getSize());
                // 设置 Content type, 默认是 application/octet-stream
                objectMetadata.setContentType("image/jpeg");
                PutObjectResult putObjectResult = cosClient.putObject(bucketName, key, fileInputStream, objectMetadata);

                GeneratePresignedUrlRequest req =
                        new GeneratePresignedUrlRequest(bucketName, key, HttpMethodName.GET);
                URL url = cosClient.generatePresignedUrl(req);
                String authority = url.getAuthority();
                String path = url.getPath();
                System.out.println("path====>" + path);
                return "https://" + authority + path;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @GetMapping("/deleteImage")
    public String deleteImage(String url) {
        // 1 初始化用户身份信息（secretId, secretKey）。
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        // 2 设置bucket的区域, COS地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        // clientConfig中包含了设置 region, https(默认 http), 超时, 代理等 set 方法, 使用可参见源码或者常见问题 Java SDK 部分。
        ClientConfig clientConfig = new ClientConfig(new Region("ap-guangzhou"));
        // 3 生成 cos 客户端。
        COSClient cosClient = new COSClient(cred, clientConfig);

        // Bucket的命名格式为 BucketName-APPID ，此处填写的存储桶名称必须为此格式
        //https://wayenbucket1-1257014484.cos.ap-guangzhou.myqcloud.com/e6719834-99c3-482c-9ba1-81be46c0bc85timg%20(6).jpg

        String[] split = url.split("/");
        String s = split[split.length - 1];

        cosClient.deleteObject(bucketName, s);
        return "SUCCESS";
    }
}
