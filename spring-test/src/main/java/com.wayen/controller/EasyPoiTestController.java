package com.wayen.controller;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import cn.afterturn.easypoi.handler.inter.IExcelDataHandler;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wayen.handler.UserExcelHandler;
import com.wayen.model.Goods;
import com.wayen.model.User;
import com.wayen.util.FileUtil;
import com.wayen.util.RedisUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2019/5/18.
 */

@RequestMapping("/testpoi")
@RestController
public class EasyPoiTestController {

    private static final Logger log = LoggerFactory.getLogger(EasyPoiTestController.class);

    @Autowired
    RedisUtils redisUtils;

    @PostMapping("/excelImport")
    public void excelImport(@RequestParam("file") MultipartFile file, HttpServletResponse response) {

        long size = file.getSize();
        ImportParams importParams = new ImportParams();
        // 数据处理
        IExcelDataHandler<User> handler = new UserExcelHandler();
        handler.setNeedHandlerFields(new String[]{"姓名"});// 注意这里对应的是excel的列名。也就是对象上指定的列名。
        importParams.setDataHanlder(handler);

        // 需要验证
        importParams.setNeedVerfiy(true);
        importParams.setTitleRows(6);
        importParams.setHeadRows(1);
        importParams.setStartRows(0);

        try {
            InputStream inputStream = file.getInputStream();
            ExcelImportResult<User> result = ExcelImportUtil.importExcelMore(inputStream, User.class,
                    importParams);
            Workbook failWorkbook = result.getFailWorkbook();

            String filename = "D://测试.xlsx";
            failWorkbook.write(new FileOutputStream(new File(filename)));

            List<User> successList = result.getList();
            List<User> failList = result.getFailList();

            log.info("是否存在验证未通过的数据:" + result.isVerfiyFail());
            log.info("验证通过的数量:" + successList.size());
            log.info("验证未通过的数量:" + failList.size());

            for (User user : successList) {
                log.info("成功列表信息:ID=" + user.getId() + user.getName() + "-"
                        + new SimpleDateFormat("yyyy-MM-dd").format(user.getBirthday()));
            }
            for (User user : failList) {
                log.info("失败列表信息:" + user.getName());
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @GetMapping(value = "/export")
    public void export(HttpServletResponse response) throws ParseException {
        List<User> list = new ArrayList<>();
        User user1 = new User();
        user1.setName("小刘");
        user1.setAge(18);
//        user1.setBirthday(new Date("2015-12-03"));
        list.add(user1);

        User user2 = new User();
        user2.setName("小王");
        user2.setAge(19);
//        user2.setBirthday(new Date("2015-12-04"));
        list.add(user2);

        User user3 = new User();
        user3.setName("小赵");
        user3.setAge(15);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date parse = format.parse("2015-09-23");
        user3.setBirthday(parse);
//        user3.setBirthday(new Date("2016-12-04"));
        list.add(user3);

        //导出操作
        FileUtil.exportExcel(list, "通讯录", "技术部", User.class, "通讯录.csv", response);
    }

    @PostMapping("/importGoods")
    public String importGoods(@RequestParam("file") MultipartFile file) {
        long size = file.getSize();
        log.info("" + size);
        ImportParams importParams = new ImportParams();
        // 需要验证
        importParams.setNeedVerfiy(true);
//        importParams.setTitleRows(1);
//        importParams.setHeadRows(1);
//        importParams.setStartRows(2);
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-hh:mm:ss");
        String s = dateFormat.format(date);

        try {
            InputStream inputStream = file.getInputStream();
            ExcelImportResult<Goods> importResult = ExcelImportUtil.importExcelMore(inputStream, Goods.class, importParams);

            Workbook failWorkbook = importResult.getFailWorkbook();
            failWorkbook.write(new FileOutputStream(new File("D://商品错误数据.xlsx")));

            List<Goods> failList = importResult.getFailList();
            redisUtils.set(s, JSONObject.toJSONString(failList));
            log.info("failList====>" + JSONObject.toJSONString(failList));
            List<Goods> list = importResult.getList();
            log.info("list====>" + JSONObject.toJSONString(list));

//            log.info("是否存在验证未通过的数据:" + result.isVerfiyFail());
//            log.info("验证通过的数量:" + successList.size());
//            log.info("验证未通过的数量:" + failList.size());

//            for (Goods goods : successList) {
//                log.info("成功列表信息:ID=" + JSONObject.toJSONString(goods));
//            }
//            for (Goods goods : failList) {
//                log.info("失败列表信息:" + JSONObject.toJSONString(goods));
//            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return s;
    }

    @GetMapping("/getWorkBook")
    public void getWorkBook(String s, HttpServletResponse response) {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
        String format = dateFormat.format(date);

        Object o = redisUtils.get(s);
        String s2 = o.toString();
        log.info(o.toString());
        String s1 = JSONObject.toJSONString(o);
        log.info("s1====>" + s1);

//        List<Goods> goods = JSONArray.parseArray(s1, Goods.class);
        List<Goods> goods = JSONArray.parseArray(s2, Goods.class);
        log.info("goods====>" + goods);
        //导出操作
        FileUtil.exportExcel(goods, "导入失败的商品", "", Goods.class, format + "导入失败的商品.csv", response);

    }


}
