package com.wayen.controller;

import com.wayen.util.ImageUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ImageTestController {


    @GetMapping("/imageInfo")
    public String imageInfo(String url) {
        ImageUtil.getImageInfoByUrl(url);
        return "SUCCESS";
    }
}
