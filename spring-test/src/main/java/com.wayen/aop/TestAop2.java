package com.wayen.aop;

import com.alibaba.fastjson.JSONObject;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class TestAop2 {

    /**
     * 2. 指定切点
     * 匹配com.zdj.springboot_aop.Controller包及其子包下面的所有类的所有方法
     */
    @Pointcut("execution(* com.wayen.service.impl.TestServiceImpl.hello(..))")
    public void executeService() {

    }

    /**
     * 环绕通知：
     * 环绕通知非常强大，可以决定目标方法是否执行，什么时候执行，执行时是否需要替换方法参数，执行完毕是否需要替换返回值。
     * 环绕通知第一个参数必须是org.aspectj.lang.ProceedingJoinPoint类型
     */
    @Around("executeService()")
    public Object doAroundService(ProceedingJoinPoint proceedingJoinPoint) {
        System.out.println("环绕通知2的目标方法名为 ： " + proceedingJoinPoint.getSignature().getName());

        try {
            Object object = proceedingJoinPoint.proceed();
            System.out.println("环绕通知2====》" + JSONObject.toJSONString(object));
            return object;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }

}
