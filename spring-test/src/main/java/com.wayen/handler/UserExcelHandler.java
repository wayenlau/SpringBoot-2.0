package com.wayen.handler;

import cn.afterturn.easypoi.handler.impl.ExcelDataHandlerDefaultImpl;
import com.alibaba.fastjson.JSONObject;
import com.wayen.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Administrator on 2019/5/18.
 */
public class UserExcelHandler extends ExcelDataHandlerDefaultImpl<User> {

    private static final Logger log = LoggerFactory.getLogger(UserExcelHandler.class);


    @Override
    public Object importHandler(User obj, String name, Object value) {
        log.info(name+":"+value);
        return super.importHandler(obj, name, value);
    }
}
