package com.wayen;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * 说明：启动类
 *
 * @EnableCaching 注解配置启用缓存，自动配置配置文件的配置信息进行条件注入缓存所需实例
 */
@ServletComponentScan("com.wayen.filter")
@SpringBootApplication
@MapperScan("com.wayen.mapper")
//@EnableCaching
public class WayenApplication {

    public static void main(String[] args) {
        SpringApplication.run(WayenApplication.class, args);
    }

}