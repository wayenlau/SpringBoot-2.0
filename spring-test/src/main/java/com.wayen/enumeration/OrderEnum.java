package com.wayen.enumeration;

public enum OrderEnum {
    ORDER_CONFIRMING(Byte.valueOf("1")),//待确认
    ORDER_WAIT_PAYMENT(Byte.valueOf("2")),//待付款
    ORDER_WAIT_SEND_DELIVERY(Byte.valueOf("3")),//待发货
    ORDER_WAIT_TAKE_DELIVERY(Byte.valueOf("4")),//待收货
    ORDER_CONFIRM_RECEIVE(Byte.valueOf("5")),//确认收货
    ORDER_EVALUATED(Byte.valueOf("6")),//已评价
    ORDER_ALREADY_RETURN(Byte.valueOf("7")),//已退货
    ORDER_ALREADY_CANCEL(Byte.valueOf("8")),//已取消
    ORDER_COMPLETE(Byte.valueOf("9")),//已完成
    ORDER_CLOSED(Byte.valueOf("10"));//已关闭

    private Byte status;

    OrderEnum(Byte status) {
        this.status = status;
    }

    public Byte status() {
        return status;
    }
}
