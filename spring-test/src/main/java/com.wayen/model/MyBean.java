package com.wayen.model;

public class MyBean {

    public MyBean() {
        System.out.println("myBean的构造方法====>generate MyBean Instance");
    }

    public void init() {
        System.out.println("myBean的init方法=====>MyBean Resources Initialized");
    }

    public String doSomthing() {

        return "hello-world";
    }
}
