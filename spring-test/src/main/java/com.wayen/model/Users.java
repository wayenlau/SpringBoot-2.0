package com.wayen.model;

import java.io.Serializable;

public class Users implements Serializable {
    /**
     * 
     */
    private Integer uId;

    /**
     * 
     */
    private String uUsername;

    /**
     * 
     */
    private String uPassword;

    private static final long serialVersionUID = 1L;

    public Integer getuId() {
        return uId;
    }

    public Users withuId(Integer uId) {
        this.setuId(uId);
        return this;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public String getuUsername() {
        return uUsername;
    }

    public Users withuUsername(String uUsername) {
        this.setuUsername(uUsername);
        return this;
    }

    public void setuUsername(String uUsername) {
        this.uUsername = uUsername;
    }

    public String getuPassword() {
        return uPassword;
    }

    public Users withuPassword(String uPassword) {
        this.setuPassword(uPassword);
        return this;
    }

    public void setuPassword(String uPassword) {
        this.uPassword = uPassword;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", uId=").append(uId);
        sb.append(", uUsername=").append(uUsername);
        sb.append(", uPassword=").append(uPassword);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}