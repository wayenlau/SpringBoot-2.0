package com.wayen.model.Enum;

/**
 * Created by Administrator on 2019/5/30.
 */
public enum JobEnum implements BaseEnum<JobEnum> {

    Administrator("0", "管理员"),
    sell("1", "销售"),
    Service("2", "客服"),
    Finance("3", "财务"),
    Worker("4", "工人");

    private String value;

    private String label;

    private JobEnum(String value, String label) {
        this.value = value;
        this.label = label;
    }

    @Override
    public String toString() {
        return display();
    }
}
