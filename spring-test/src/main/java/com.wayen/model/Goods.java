package com.wayen.model;

import cn.afterturn.easypoi.excel.annotation.Excel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class Goods {


    /**
     * 商品名称/药品商品名
     */
    @Excel(name = "*商品标题")
    @NotBlank(message = "商品标题不能为空")
    public String goodsName;

    /**
     * 卖点
     */
    @Excel(name = "商品卖点")
    public String goodsPromote;

    @Excel(name = "省")
    @NotBlank(message = "省不能为空")
    public String locationProvince;

    /**
     * 所在地(市)
     */
    @Excel(name = "城市")
    @NotBlank(message = "城市不能为空")
    public String locationCity;

    /**
     * 所在地(区)
     */
    @Excel(name = "区")
    @NotBlank(message = "区不能为空")
    public String locationArea;

    /**
     * 手机详情页
     */

    /**
     * 运费模板id
     */

    /**
     * 商品图片
     */

    /**
     * 药品名称
     */
    @Excel(name = "*药品名称")
    @NotBlank(message = "药品名称不能为空")
    public String medicineName;

    /**
     * 规格
     */
    @Excel(name = "*规格")
    @NotBlank(message = "规格不能为空")
    public String specModel;

    /**
     * 用法
     */
    @Excel(name = "*用法")
    @NotBlank(message = "用法不能为空")
    public String direction;

    /**
     * 批准文号
     */

    @Excel(name = "*批准文号")
    @NotBlank(message = "批准文号不能为空")
    public String approvalNumber;

    /**
     * 通用名称
     */
    @Excel(name = "*药品通用名")
    @NotBlank(message = "药品通用名不能为空")
    public String genericName;

    /**
     * 药品商品名
     */
    @Excel(name = "药品商品名")
    public String commodityName;

    /**
     * 生产企业
     */
    @NotBlank(message = "生产企业不能为空")
    @Excel(name = "*生产企业")
    public String productionEnterprise;

    /**
     * 有效期(如:36个月)
     */
    @NotBlank(message = "有效期不能为空")
    @Excel(name = "*有效期")
    public String validDate;

    /**
     * 产品剂型
     */
    @NotBlank(message = "产品剂型不能为空")
    @Excel(name = "*产品剂型")
    public String dosage;

    @NotBlank(message = "使用剂量不能为空")
    @Excel(name = "*使用剂量")
    public String userDosage;

    /**
     * 疾病
     */
    @NotBlank(message = "疾病不能为空")
    @Excel(name = "*疾病")
    public String indication;

    /**
     * 症状
     */
    @Excel(name = "症状")
    public String shape;

    /**
     * 药品类别
     */
    @NotBlank(message = "药品类别不能为空")
    @Excel(name = "*药品类别")
    public String genre;

    /**
     * 药品分类是否处方药(0:非处方药,1:处方药)
     */
    @Excel(name = "药品分类")
    public String isPrescription;

    /**
     * 适合人群
     */
    @NotBlank(message = "适合人群不能为空")
    @Excel(name = "*适合人群")
    public String suitCrowds;

    /**
     * 商品码
     */
    @Excel(name = "商家编码")
    public String goodsCode;

    /**
     * 条形码
     */
    @NotBlank(message = "条形码不能为空")
    @Excel(name = "*条形码")
    public String barCode;

    /**
     * 品牌
     */
    @NotBlank(message = "品牌不能为空")
    @Excel(name = "*品牌")
    public String brand;

    /**
     * 透明素材
     */

    /**
     * 物流重量
     */
    @Excel(name = "物流重量")
    public String weight;

    /**
     * 物流体积
     */
    @Excel(name = "物流体积")
    public String volume;

    /**
     * 规格库存名称
     */
    @NotBlank(message = "套餐类型不能为空")
    @Excel(name = "*套餐类型")
    public String specDescribe;

    /**
     * 价格
     */
    @NotNull(message = "价格不能为空")
    @Excel(name = "*价格")
    public BigDecimal price;

    /**
     * 库存
     */
    @NotNull(message = "数量不能为空")
    @Excel(name = "*数量")
    public Integer stock;

    /**
     * 编码
     */
    @Excel(name = "套餐商家编码")
    public String code;

    /**
     * 操作(0、启用   1、不启用)
     */
    @NotNull(message = "是否启用不能为空")
    @Excel(name = "*操作")
    public Integer enable;

    /**
     * 规格库存名称
     */
    @Excel(name = "套餐类型2")
    public String specDescribe2;

    /**
     * 价格
     */
    @Excel(name = "价格2")
    public BigDecimal price2;

    /**
     * 库存
     */
    @Excel(name = "数量2")
    public Integer stock2;

    /**
     * 编码
     */
    @Excel(name = "商家编码2")
    public String code2;

    /**
     * 操作(0、启用   1、不启用)
     */
    @Excel(name = "操作2")
    public Integer enable2;

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsPromote() {
        return goodsPromote;
    }

    public void setGoodsPromote(String goodsPromote) {
        this.goodsPromote = goodsPromote;
    }

    public String getLocationProvince() {
        return locationProvince;
    }

    public void setLocationProvince(String locationProvince) {
        this.locationProvince = locationProvince;
    }

    public String getLocationCity() {
        return locationCity;
    }

    public void setLocationCity(String locationCity) {
        this.locationCity = locationCity;
    }

    public String getLocationArea() {
        return locationArea;
    }

    public void setLocationArea(String locationArea) {
        this.locationArea = locationArea;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public String getSpecModel() {
        return specModel;
    }

    public void setSpecModel(String specModel) {
        this.specModel = specModel;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getApprovalNumber() {
        return approvalNumber;
    }

    public void setApprovalNumber(String approvalNumber) {
        this.approvalNumber = approvalNumber;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    public String getProductionEnterprise() {
        return productionEnterprise;
    }

    public void setProductionEnterprise(String productionEnterprise) {
        this.productionEnterprise = productionEnterprise;
    }

    public String getValidDate() {
        return validDate;
    }

    public void setValidDate(String validDate) {
        this.validDate = validDate;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getUserDosage() {
        return userDosage;
    }

    public void setUserDosage(String userDosage) {
        this.userDosage = userDosage;
    }

    public String getIndication() {
        return indication;
    }

    public void setIndication(String indication) {
        this.indication = indication;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getIsPrescription() {
        return isPrescription;
    }

    public void setIsPrescription(String isPrescription) {
        this.isPrescription = isPrescription;
    }

    public String getSuitCrowds() {
        return suitCrowds;
    }

    public void setSuitCrowds(String suitCrowds) {
        this.suitCrowds = suitCrowds;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getSpecDescribe() {
        return specDescribe;
    }

    public void setSpecDescribe(String specDescribe) {
        this.specDescribe = specDescribe;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public String getSpecDescribe2() {
        return specDescribe2;
    }

    public void setSpecDescribe2(String specDescribe2) {
        this.specDescribe2 = specDescribe2;
    }

    public BigDecimal getPrice2() {
        return price2;
    }

    public void setPrice2(BigDecimal price2) {
        this.price2 = price2;
    }

    public Integer getStock2() {
        return stock2;
    }

    public void setStock2(Integer stock2) {
        this.stock2 = stock2;
    }

    public String getCode2() {
        return code2;
    }

    public void setCode2(String code2) {
        this.code2 = code2;
    }

    public Integer getEnable2() {
        return enable2;
    }

    public void setEnable2(Integer enable2) {
        this.enable2 = enable2;
    }
}
