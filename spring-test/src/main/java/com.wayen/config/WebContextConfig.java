package com.wayen.config;

import com.wayen.convert.StringToBaseEnumConverterFactory;
import com.wayen.interceptor.LogInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by Administrator on 2018/4/10.
 */
@Configuration
public class WebContextConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverterFactory(new StringToBaseEnumConverterFactory());
    }

    /**
     * 配置静态资源访问
     *
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/AdminLTE-2.4.3/**").addResourceLocations("classpath:/AdminLTE-2.4.3/");
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        //windows
        registry.addResourceHandler("/images/**").addResourceLocations("file:E:/images/");
        //linux
//        registry.addResourceHandler("/images/**").addResourceLocations("file:/usr/local/images/");
        registry.addResourceHandler("/dist/**").addResourceLocations("classpath:/dist/");
        super.addResourceHandlers(registry);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LogInterceptor()).addPathPatterns("/**");
        super.addInterceptors(registry);
    }
}
