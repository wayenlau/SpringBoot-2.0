package com.wayen.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Administrator on 2019/5/16.
 */
@Configuration
public class CarConfiguration {


    /**
     * 发动机
     */
    public static class Engine {
        public Engine() {
            System.out.println("====================================================");
            System.out.println("Engine构造方法");

        }
    }


    /**
     * 汽车
     */
    public static class Car {
        private String name;
        private Engine engine;

        public Car(String name,Engine engine) {
            System.out.println("=========================================================");
            System.out.println("Car(String name,Engine engine)构造方法==>"+name);
            this.name = name;
            this.engine = engine;
        }

        public String getName() {
            return name;
        }
    }


    /**
     * 造一个引擎
     */

    @Bean
    public Engine engine() {

        return new Engine();

    }


    /**
     * 造一辆 LP 700-4
     */

    @Bean
    public Car aventador() {

        return new Car("LP700-4", engine());

    }


    /**
     * 造一辆 X6
     */

    @Bean
    public Car bmw() {

        return new Car("X6", engine());

    }
}
