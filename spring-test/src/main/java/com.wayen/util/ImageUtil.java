package com.wayen.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class ImageUtil {

    private static final Logger logger = LoggerFactory.getLogger(ImageUtil.class);

    public static void getImageInfoByUrl(String urlStr) {
        try {
            URL url = new URL(urlStr);
            URLConnection urlConnection = url.openConnection();
            int length = urlConnection.getContentLength();
            System.out.print("length: " + length);
            if (length == -1) {
                System.out.print("image not exist");
            } else {
                BufferedImage sourceImg = ImageIO.read(url);
                System.out.print("image width:" + sourceImg.getWidth());
                System.out.print("image height:" + sourceImg.getHeight());
            }
        } catch (MalformedURLException e) {
            logger.info("url not right " + e.getMessage(), e);
        } catch (IOException e) {
            logger.info("IOException " + e.getMessage(), e);
        }
    }
}
