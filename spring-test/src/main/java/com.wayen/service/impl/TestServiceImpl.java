package com.wayen.service.impl;

import com.wayen.service.TestService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Administrator on 2019/5/20.
 */
@Service
public class TestServiceImpl implements TestService {

    /**
     *     REQUIRED(0),支持当前事务，如果当前没有事务，就新建一个事务。这是最常见的选择
     *     SUPPORTS(1),支持当前事务，如果当前没有事务，就以非事务方式执行。
     *     MANDATORY(2),支持当前事务，如果当前没有事务，就抛出异常。
     *     REQUIRES_NEW(3),新建事务，如果当前存在事务，把当前事务挂起。
     *     NOT_SUPPORTED(4),以非事务方式执行操作，如果当前存在事务，就把当前事务挂起。
     *     NEVER(5),以非事务方式执行，如果当前存在事务，则抛出异常。
     *     NESTED(6);如果当前存在事务，则在嵌套事务内执行。如果当前没有事务，则进行与REQUIRED类似的操作。
     * @param name
     * @param age
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public String hello(String name, int age) {
        System.out.println("TestServiceImpl.hello()============>");
        int i = age / 5;
        if (age == 5) {
            int i1 = age / 0;
        }
        Integer in = 10;
        int i1 = 10 / 5;
        return "hello-world-" + name;
    }


}