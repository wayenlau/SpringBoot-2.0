package com.wayen.convert;

import com.wayen.model.Enum.BaseEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;
import org.springframework.util.StringUtils;

/**
 * 自定义转换器
 */
public class StringToBaseEnumConverterFactory implements ConverterFactory<String, BaseEnum> {
    private static final Logger logger = LoggerFactory.getLogger(StringToBaseEnumConverterFactory.class);

    @Override
    public <T extends BaseEnum> Converter<String, T> getConverter(Class<T> targetType) {
        return new StringToEnumConverter(targetType);
    }

    private final class StringToEnumConverter<T extends BaseEnum> implements Converter<String, T> {

        private Class<T> enumType;

        public StringToEnumConverter(Class<T> enumType) {
            this.enumType = enumType;
        }

        public T convert(String source) {
            if (StringUtils.isEmpty(source)) {
                return null;
            }
            T baseEnum = BaseEnum.valueOfEnum(this.enumType, source.trim());
            if (baseEnum == null) {
                logger.warn("枚举类{} value为{}的枚举不存在", enumType.getSimpleName(), source);
            }
            return baseEnum;
        }

    }
}
