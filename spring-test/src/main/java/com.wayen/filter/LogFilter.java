package com.wayen.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * 过滤器
 */
@WebFilter(urlPatterns = "/*", filterName = "logFilter")
public class LogFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(LogFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("过滤器初始化方法====>");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        logger.info("过滤器dofilter方法");

        long start = System.currentTimeMillis();
        filterChain.doFilter(servletRequest, servletResponse);

        System.out.println("LogFilter2 Execute cost=" + (System.currentTimeMillis() - start));

    }

    @Override
    public void destroy() {
        logger.info("过滤器destory方法====》");
    }
}
